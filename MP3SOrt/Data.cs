﻿namespace MP3SOrt
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using Mp3;

    public class Data
    {
        public Data()
        {
            MP3Hilf mp3Hilf = new MP3Hilf();
            mp3Hilf.ReadFiles();
            this.OtherData = mp3Hilf.AndereFilesListe;
            this.ErrorList = mp3Hilf.FehlerListe;
            this.Mp3DataList = mp3Hilf.Mp3InfoListe;
        }

        public ObservableCollection<Mp3Info> Mp3DataList { get; set; }

        public List<string> ErrorList { get; set; }

        public List<string> OtherData { get; set; }
    }
}