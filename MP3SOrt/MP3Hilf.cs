﻿namespace MP3SOrt
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;

    using Mp3;

    internal class MP3Hilf
    {
        #region Instanz Variablen

        private readonly string searchpattern = "*.mp3";

        private readonly string path = string.Empty;

        private ObservableCollection<Mp3Info> mp3InfoListe;

        private List<string> andereFilesListe;

        private List<string> fehlerListe;

        #endregion Instanz Variablen

        #region Konstruktoren

        public MP3Hilf(string searchpattern = "*.mp3", string path = "H:/Eigene Dateien/Eigene Musik")
        {
            this.searchpattern = searchpattern;
            this.path = path;

            this.fehlerListe = new List<string>();
            this.mp3InfoListe = new ObservableCollection<Mp3Info>();
            this.andereFilesListe = new List<string>();
        }

        #endregion Konstruktoren

        #region Properties

        public List<string> FehlerListe
        {
            get
            {
                return this.fehlerListe;
            }

            set
            {
                this.fehlerListe = value;
            }
        }

        public ObservableCollection<Mp3Info> Mp3InfoListe
        {
            get
            {
                return this.mp3InfoListe;
            }

            set
            {
                this.mp3InfoListe = value;
            }
        }

        public List<string> AndereFilesListe
        {
            get
            {
                return this.andereFilesListe;
            }

            set
            {
                this.andereFilesListe = value;
            }
        }

        #endregion Properties

        #region Methoden

        public void ReadFiles1()
        {
            this.fehlerListe = new List<string>();
            this.mp3InfoListe = new ObservableCollection<Mp3Info>();
            this.andereFilesListe = new List<string>();

            try
            {
                foreach (string d in Directory.GetDirectories(this.path))
                {
                    string[] allFiles = Directory.GetFiles(d, this.searchpattern);

                    for (int i = 0; i < allFiles.Length; i++)
                    {
                        Mp3Info mp3Info = new Mp3Info();
                        this.NewMethod(allFiles, mp3Info, i);
                    }
                }
            }
            catch (Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }

        public void ReadFiles(DirectoryInfo di = null)
        {
            if (di == null)
            {
                di = new DirectoryInfo(this.path);
            }

            try
            {
                // Alle Verzeichnisse rekursiv durchlaufen
                foreach (DirectoryInfo subdir in di.GetDirectories())
                {
                    this.ReadFiles(subdir);
                }

                // Alle Dateien durchlaufen
                foreach (FileInfo fi in di.GetFiles(this.searchpattern))
                {
                    Mp3Info mp3Info = new Mp3Info();
                    try
                    {
                        mp3Info.Read(fi.FullName);
                        if (mp3Info.ContainsID3v1Information)
                        {
                            this.mp3InfoListe.Add(mp3Info);
                        }
                        else
                        {
                            this.andereFilesListe.Add(
                                string.Format("File '{0}' does not contain ID3v1 information.", fi.FullName));
                        }
                    }
                    catch (Exception e)
                    {
                        this.fehlerListe.Add("Error: " + e.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void NewMethod(string[] allFiles, Mp3Info mp3Info, int i)
        {
            try
            {
                mp3Info.Read(allFiles[i]);
                if (mp3Info.ContainsID3v1Information)
                {
                    this.mp3InfoListe.Add(mp3Info);
                }
                else
                {
                    this.andereFilesListe.Add(
                        string.Format("File '{0}' does not contain ID3v1 information.", allFiles[i]));
                }
            }
            catch (Exception e)
            {
                this.fehlerListe.Add("Error: " + e.ToString());
            }
        }

        #endregion Methoden

        public override bool Equals(object obj)
        {
            return this.FehlerListe.Equals(((MP3Hilf)obj).FehlerListe);
        }
    }
}