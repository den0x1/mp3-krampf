﻿namespace MP3SOrt
{
    using System.Windows;

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Data data = null;

        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            this.data = this.FindResource("Data") as Data;
        }
    }
}